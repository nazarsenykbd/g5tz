import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TEMP {

    public static void main(String[] args) throws IOException {
        parseData();
        System.out.println("Successfully parsed and created output.tsv file ");
    }

    public static Person createHeaders(String line) {
        String[] columns = line.split("\\W+");
        return new Person(columns[0], columns[1], columns[2], columns[3], columns[4]);
    }

    public static Person createPerson(String line, BufferedReader br) throws IOException {
        // make line with all related data to one person
        while (!isFullOfData(line)) {
            line = line.concat(br.readLine());
        }
//        System.out.println(line);
        String id = getIdFromLine(line);
        String first_name = getFNFromLine(line);
        String last_name = getLNFromLine(line);
        String account_number = getAccFromLine(line);
        String email = getEmailFromLine(line);

        return new Person(id, first_name, last_name, account_number, email);
    }

    public static String getFullNameFromLine(String line) {
        Pattern patternNAME = Pattern.compile("\\d\\s([\\s[a-zA-ZÈ-]+]+)\\s\\d");
        Matcher matcher = patternNAME.matcher(line);
        if (matcher.find()) {
            String name = matcher.group(1);
            return name;
        }
        return null;
    }

    public static String getFNFromLine(String line) {
        String fullName = getFullNameFromLine(line);

        Pattern p = Pattern.compile("([a-zA-ZÈ-]+)");
        Matcher m = p.matcher(fullName);
        String name = "";
        if (m.find()) {
            name = name.concat(m.group());
        }
        return name;
    }

    public static String getLNFromLine(String line) {
        String fullName = getFullNameFromLine(line);

        Pattern p = Pattern.compile("([a-zA-ZÈ-]+)");
        Matcher m = p.matcher(fullName);
        String name = "";
        if (m.find()) {
            if (m.find())
                name = name.concat(m.group());
        }
        return name;
    }

    public static String getIdFromLine(String line) {
        Pattern patternID = Pattern.compile("(^\\d+)");
        Matcher matcher = patternID.matcher(line);
        if (matcher.find()) {
            return matcher.group(1);
        } else return null;
    }

    public static String getAccFromLine(String line) {
        Pattern patternACCOUNT = Pattern.compile("\\s(\\d{6})");
        Matcher matcher = patternACCOUNT.matcher(line);
        if (matcher.find()) {
            return matcher.group(1);
        } else return null;
    }

    public static String getEmailFromLine(String line) {
        Pattern patternEMAIL = Pattern.compile("([a-zA-Z0-9_\\-\\.]+@[a-zA-Z0-9_\\-\\.]+\\.[a-zA-Z]{2,5})$");
        Matcher matcher = patternEMAIL.matcher(line);
        if (matcher.find()) {
            return matcher.group(1);
        } else return null;
    }

    public static boolean isFullOfData(String line) {
        Pattern patternEMAIL = Pattern.compile("([a-zA-Z0-9_\\-\\.]+@[a-zA-Z0-9_\\-\\.]+\\.[a-zA-Z]{2,5})$");
        Matcher matche2 = patternEMAIL.matcher(line);
        if (matche2.find()) {
            return true;
        }
        return false;
    }

    public static void parseData() throws IOException {
        String fileName = "data.tsv";
        File file = new File(fileName);

        List<Person> personList = new LinkedList<>();

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_16LE));
        try {

            Person header = createHeaders(br.readLine());
            personList.add(header);

            StringBuilder sb = new StringBuilder();

            String line = br.readLine();

            while (line != null) {
                personList.add(createPerson(line, br));
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }

            String transformedData = getStringFromPersonsList(personList);

            FileOutputStream fs = new FileOutputStream(new File("output.tsv"));
            fs.write(transformedData.getBytes("UTF-8"));
            fs.close();

        } finally {
            br.close();
        }

    }

    public static String getStringFromPersonsList(List<Person> personList) {
        StringBuilder sb = new StringBuilder();
        String prefix = "";

        for (Person element : personList) {
            sb.append(prefix);
            prefix = "\n";
            sb.append(element.toString());
        }

        return sb.toString();
    }
}


