public class Person {
    private String id;
    private String first_name;
    private String last_name;
    private String account_number;
    private String email;

    public Person(String id, String first_name, String last_name, String account_number, String email) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.account_number = account_number;
        this.email = email;
    }

    public Person() {
    }

    @Override
    public String toString() {
        return  id + '\t' +
                first_name + '\t' +
                last_name + '\t' +
                account_number + '\t' +
                email;
    }
}
