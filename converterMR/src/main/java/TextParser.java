import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TextParser {

    public static List<Object> buildList(String[] data, List<ColumnDatatypeMapping> mapping) {

        List<Object> struct = new ArrayList<Object>();
        try {
            for (int i = 0; i < mapping.size(); i++) {
                String dataType = mapping.get(i).colType;
                String colData = data[i];
                if (dataType.toLowerCase().startsWith("tiny")) {
                    struct.add(Integer.valueOf(colData));
                } else if (dataType.toLowerCase().startsWith("small")) {
                    struct.add(Integer.valueOf(colData));
                } else if (dataType.toLowerCase().startsWith("int")) {
                    struct.add(Integer.valueOf(colData));
                } else if (dataType.toLowerCase().startsWith("big")) {
                    struct.add(Integer.valueOf(colData));
                } else if (dataType.toLowerCase().startsWith("double")) {
                    struct.add(Double.valueOf(colData));
                } else if (dataType.toLowerCase().startsWith("decimal")) {
                    struct.add(Float.valueOf(colData));
                } else if (dataType.toLowerCase().startsWith("string")) {
                    struct.add(colData);
                } else if (dataType.toLowerCase().startsWith("varchar")) {
                    struct.add(colData);
                } else if (dataType.toLowerCase().startsWith("char")) {
                    struct.add(colData);
                } else if (dataType.toLowerCase().startsWith("bool")) {
                    struct.add(Boolean.valueOf(colData));
                } else if (dataType.toLowerCase().startsWith("binary")) {
                    struct.add(Byte.valueOf(colData));
                } else if (dataType.toLowerCase().startsWith("date")) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    struct.add(new java.sql.Date(formatter.parse(colData).getTime()));
                } else {
                    struct.add(colData); //keep default data type as string
                }
            }
        } catch (Exception e) {
            System.err.println("Skipped row : " + data.toString());
            e.printStackTrace();
        }
        return struct;
    }

}