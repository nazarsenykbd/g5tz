public class ColumnDatatypeMapping {
    String colName;
    String colType;

    public ColumnDatatypeMapping(String colName, String colType) {
        super();
        this.colName = colName;
        this.colType = colType;
    }
}