import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hive.ql.io.orc.OrcSerde;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;

public class FileMapper extends Mapper<LongWritable, Text, NullWritable, Writable> {


    private OrcSerde serde;
    private String types;
    private TypeInfo typeInfo;
    private ObjectInspector objectInspector;
    private List<Object> struct;
    private ArrayList<ColumnDatatypeMapping> mapping;

    @Override
    protected void setup(Mapper<LongWritable, Text, NullWritable, Writable>.Context context) {

        serde = new OrcSerde();
        types = "struct<id:int,first_name:string,last_name:string,account_number:int,email:string>";
        typeInfo = TypeInfoUtils.getTypeInfoFromTypeString(types);
        objectInspector = TypeInfoUtils.getStandardJavaObjectInspectorFromTypeInfo(typeInfo);
        mapping = new ArrayList<ColumnDatatypeMapping>();

        //create mapping, in the same order as in the file

        mapping.add(new ColumnDatatypeMapping("id", "int"));
        mapping.add(new ColumnDatatypeMapping("first_name", "string"));
        mapping.add(new ColumnDatatypeMapping("last_name", "string"));
        mapping.add(new ColumnDatatypeMapping("account_number", "int"));
        mapping.add(new ColumnDatatypeMapping("email", "string"));

    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String delimiter = "\t";
        String[] data = value.toString().split(delimiter);
        struct = TextParser.buildList(data, mapping);
        Writable row = serde.serialize(struct, objectInspector);
        context.write(NullWritable.get(), row);
    }


}